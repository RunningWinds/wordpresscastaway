    <footer>
        <div class="container d-flex justify-content-between" style="margin-top:1rem;">
            <div>
                <a class="logo" href="/">Castaway</a>
                <div>
                    <a href="/"><i class="fa-brands fa-instagram fa-xl" aria-hidden="true"></i></a>
                    <a href="/"><i class="fa-brands fa-twitter fa-xl" aria-hidden="true"></i></a>
                </div>
            </div>
            <?php wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'footerNavMenu navbar-nav')); ?>
            <div>
                <a href="/"><i class="fa-brands fa-soundcloud fa-xl" aria-hidden="true"></i></a>
            </div>
        </div>
    </footer>
</body>
<?php wp_footer(); ?>
</html>