<?php get_header(); ?>
<div class="container">
        <div class="row justify-content-around">
            <div class="col-md-6">
            <div style="position:relative;">
                <div style="position:absolute; top:-1rem; left:-1rem;"><img src="<?php echo get_template_directory_uri() . './img/attr.png'; ?>" alt="" class="img-fluid"></div>
                <img src="<?php echo get_template_directory_uri() . './img/negr.png'; ?>" alt="negr" class="img-fluid">
            </div>
            </div>
            <div class="col-md-6 heroTitle">
                <h1>Take your podcast to the next level</h1>
                <p>Listen on <a href="/"><i class="fa-brands fa-soundcloud fa-xl" aria-hidden="true"></i></a></p>
            </div>
        </div>
    </div>

    <?php if (have_posts()):?>
    <div class="container">
        <div class="row justify-content-between" style="margin-top: 2rem; margin-bottom:1rem;">
            <h1>Latest episodes</h1>
            <button type="submit" class="btn btn-primary mb-2">View all episodes</button>
        </div>
        <?php
            $counter=0;
            $my_posts=get_posts([
                'numberposts' => 3,
                'category'    => 0,
                'orderby'     => 'date',
                'order'       => 'DESC',
                'include'     => array(),
                'exclude'     => array(),
                'meta_key'    => '',
                'meta_value'  =>'',
                'post_type'   => 'post',
                'suppress_filters' => true,
            ]);
            foreach ( $my_posts as $post ):
                setup_postdata( $post );
        ?>
        <div class="blackCover roundedAll">
            <div class="row justify-content-around">
                <div class="col-md-4">
                    <?php if (has_post_thumbnail($post->ID)): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                        <img src="<?php echo $image[0] ?>" alt="" class="img-fluid">
                    <?php endif ?>
                </div>
                <div class="col-md-8 episodeContent">
                    <a class="postLink" href="<?php echo get_post_permalink($post->ID)?>">To episode page</a>
                    <h1 class="pt-2 pb-2"><?=$post->post_title?></h1>
                    <p><?=$post->post_content?></p>
                </div>
            </div>
        </div>
        <?php $counter=$counter+1; ?>
        <?php endforeach ?>
    </div>
    <?php endif ?>

    <div class="container" style="margin-top:2rem; margin-bottom:2rem;">
        <div class="row justify-content-around">
            <div class="col-md-6 heroTitle">
                <div style="color:blue;">Meet your host</div>
                <h1>Jacob Paulaner</h1>
                <p>Jacob has a background in audio engineering, and has
                    been podcasting since the early days.
                </p>
                <p>
                    He’s here to help you level up your game by sharing
                    everything he’s learned along the way.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo get_template_directory_uri() . './img/negr2.png';?>" alt="negr2" class="img-fluid">
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 2rem;;">
        <div class="row justify-content-around">
            <div class="blackCover comment roundedAll">
                <div class="row stars">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
                <h1>I can’t recommend
                    this podcast
                    enough</h1>
                <p>Betty Lacey</p>
            </div>
            <div class="blackCover comment roundedAll">
                <div class="row stars">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
                <h1>Jacob is the best
                    in the business</h1>
                <p>Adam Driver</p>
            </div>
            <div class="blackCover comment roundedAll">
                <div class="row stars">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
                <h1>A wealth of audio
                    knowledge</h1>
                <p>Marcus Brown</p>
            </div>
            <div class="blackCover comment roundedAll">
                <div class="row stars">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
                <h1>Every episode is a
                    gem!</h1>
                <p>Jessica Knowl</p>
            </div>
        </div>
    </div>
<?php get_footer(); ?>