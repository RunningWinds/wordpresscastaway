<?php get_header();?>
<section>
	<?php while (have_posts()): the_post();?>
    <div class="container customize-support">
	    <h1><?php the_title(); ?></h1>     
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0] ?>" alt="" class="img-fluid">
		<p><?php the_content();?></p>
    </div>
	<?php endwhile; ?>
</section>
<?php get_footer(); ?>