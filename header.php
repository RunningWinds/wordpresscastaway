<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body class="customize-support">
<?php wp_head(); ?>
    <header>
        <nav class="navbar navbar-expand-md py-4" style="margin-bottom: 3rem;">
            <div class="container">
                <a class="logo" href="/">Castaway</a>
                <div class="justify-contend-end px-2">
                    <?php wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'top-menu navbar-nav navigation')); ?>
                </div>
            </div>
        </nav>
    </header>