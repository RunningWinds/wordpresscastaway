<?php
function enqueue_styles() {
	wp_register_style('first', get_template_directory_uri() . '/css/style.css', false, '1.1', 'all');
	wp_enqueue_style('first');
	wp_register_style('castaway-bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('castaway-bootstrapcss');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function add_googleicons()
{
  wp_register_script('outline','https://kit.fontawesome.com/dc4f8560f3.js');
  wp_enqueue_script('outline');
}
add_action('wp_enqueue_scripts', 'add_googleicons'); 

function enqueue_scripts () {
 wp_enqueue_script('jquery');
 wp_register_script('castaway-bootstrapbundle', get_template_directory_uri() . '/js/bootstrap.bundle.min.js');
 wp_enqueue_script('castaway-bootstrapbundle');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

remove_filter('the_content', 'wpautop');

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}

if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(311,345);
}